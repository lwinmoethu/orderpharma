package com.tamardaw.orderpharma.activity;

import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.alertdialogpro.AlertDialogPro;
import com.dd.processbutton.iml.SubmitProcessButton;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;
import com.tamardaw.orderpharma.MainApplication;
import com.tamardaw.orderpharma.adapter.FlexibleAdapter;
import com.tamardaw.orderpharma.adapter.OrderAdapter;
import com.tamardaw.orderpharma.R;

import com.tamardaw.orderpharma.anim.SlideInRightAnimator;
import com.tamardaw.orderpharma.dialog.AboutDialog;
import com.tamardaw.orderpharma.dialog.ItemDialog;
import com.tamardaw.orderpharma.model.Drug;
import com.tamardaw.orderpharma.utils.FastScroller;
import com.tamardaw.orderpharma.utils.ProgressBar;
import com.tamardaw.orderpharma.utils.Utils;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OrderActivity extends AppCompatActivity implements OrderAdapter.OnItemClickListener, ActionMode.Callback, SearchView.OnQueryTextListener, FlexibleAdapter.OnUpdateListener {


    private ProgressBar mProgressBar;

    @Bind(R.id.search)
    AutoCompleteTextView search;

    @Bind(R.id.recyclerview)
    RecyclerView rcView;
    RecyclerView.LayoutManager mLayoutManager;

    @Bind(R.id.sendOrder)
    SubmitProcessButton sendOrder;

    String drugitem;
    String[] getItem;
    List<Drug> drug;
    ParseUser user;
    int id;

    OrderAdapter mAdapter;
    private ActionMode actionMode;
    public static final String TAG = OrderActivity.class.getSimpleName();
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    private static final int INVALID_POSITION = -1;
    private int mActivatedPosition = INVALID_POSITION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mProgressBar.setVisibility(View.VISIBLE);

        getItem = MainApplication.getItem;

        mLayoutManager = new LinearLayoutManager(this);
        rcView.setLayoutManager(mLayoutManager);
        rcView.setHasFixedSize(true);
        rcView.setItemAnimator(new SlideInRightAnimator());
        mAdapter = new OrderAdapter(this, "Example parameter for list");
        mAdapter.removeAllOrders();
        rcView.setAdapter(mAdapter);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, getItem);
        search.setAdapter(adapter);


        search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                destroyActionModeIfNeeded();
                Utils.hideSoftInput(OrderActivity.this);

                //Toast.makeText(getApplicationContext(),position+" "+l+" "+((TextView)view).getText(),Toast.LENGTH_LONG).show();

                TextView item = (TextView) view;

                drugitem = item.getText().toString();
                Log.i("DrugItem", drugitem);


                drug = MainApplication.dbHelper.getInfoByItem(drugitem);
                mAdapter.addOrders(drug);
                rcView.smoothScrollToPosition(position);
                search.setText("");


                //Utils.showSoftInput(OrderActivity.this,search);
                Log.i("Durg Info", drug.toString());

            }
        });


        sendOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



             /*   sendit.fetchInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        id = sendit.getInt("orderId");
                    }
                });*/
                sendOrder.setProgress(1);
                sendOrder.setProgress(20);

                for (Drug d : mAdapter.drugs) {

                    final ParseObject sendit = new ParseObject("OrderList");
                    sendit.put("item", d.item);
                    sendOrder.setProgress(30);
                    sendit.put("size", d.size);
                    sendOrder.setProgress(40);
                    sendit.put("generic", d.generic);
                    sendOrder.setProgress(50);
                    sendit.put("company", d.company);
                    sendOrder.setProgress(60);
                    sendit.put("sendPerson", user.getCurrentUser().getUsername());
                    sendOrder.setProgress(80);
                    sendit.put("orderId", 0);
                    sendit.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e != null) {
                                Toast.makeText(OrderActivity.this, "Error : " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                sendOrder.setProgress(-1);
                            }
                        }
                    });
                }

                Toast.makeText(OrderActivity.this, "ORDERS HAVE BEEN SENT", Toast.LENGTH_SHORT).show();
                ParseQuery<ParseInstallation> query=ParseInstallation.getQuery();
                query.whereEqualTo("sale_executive",true);
                ParsePush push=new ParsePush();
                push.setQuery(query);
                push.setMessage("Hello Testing ME!");
                push.sendInBackground(new SendCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e != null) {
                            Toast.makeText(OrderActivity.this, "Sending push", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(OrderActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                sendOrder.setProgress(100);
            }
        });

        //Restore previous state
        if (savedInstanceState != null) {
            //Selection
            mAdapter.onRestoreInstanceState(savedInstanceState);
            if (mAdapter.getSelectedItemCount() > 0) {
                actionMode = startSupportActionMode(this);
                setContextTitle(mAdapter.getSelectedItemCount());
            }
            //Previously serialized activated item position
            if (savedInstanceState.containsKey(STATE_ACTIVATED_POSITION))
                setSelection(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order_activity, menu);
        initSearchView(menu);
        return true;
    }

    private void initSearchView(final Menu menu) {
        //Associate searchable configuration with the SearchView
        Log.d(TAG, "onCreateOptionsMenu setup SearchView!");
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) MenuItemCompat
                .getActionView(menu.findItem(R.id.action_search));
        searchView.setInputType(InputType.TYPE_TEXT_VARIATION_FILTER);
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_FULLSCREEN);
        searchView.setQueryHint(getString(R.string.action_search));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.findItem(R.id.action_about).setVisible(false);
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                menu.findItem(R.id.action_about).setVisible(true);
                return false;
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.v(TAG, "onPrepareOptionsMenu called!");
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        //Has searchText?
        if (!OrderAdapter.hasSearchText()) {
            Log.d(TAG, "onPrepareOptionsMenu Clearing SearchView!");
            searchView.setIconified(true);// This also clears the text in SearchView widget
        } else {
            searchView.setQuery(OrderAdapter.getSearchText(), false);
            searchView.setIconified(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onQueryTextChange(String newText) {
        if (!OrderAdapter.hasSearchText()
                || !OrderAdapter.getSearchText().equalsIgnoreCase(newText)) {
            mProgressBar.setVisibility(View.VISIBLE);
            Log.d(TAG, "onQueryTextChange newText: " + newText);
            OrderAdapter.setSearchText(newText);
//			mAdapter.updateDataSet(newText);
            mAdapter.updateDataSetAsync(newText);
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.v(TAG, "onQueryTextSubmit called!");
        return onQueryTextChange(query);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {

            AboutDialog.newInstance(R.mipmap.ic_info_grey600_24dp,
                    getString(R.string.about_title),
                    getString(R.string.about_body,
                            Utils.getVersionName(this),
                            Utils.getVersionCode(this)))
                    .show(getFragmentManager(), AboutDialog.TAG);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setSelection(final int position) {
        Log.v(TAG, "setSelection called!");

        setActivatedPosition(position);

        rcView.postDelayed(new Runnable() {
            @Override
            public void run() {
                rcView.smoothScrollToPosition(position);
            }
        }, 1000L);
    }


    private void setActivatedPosition(int position) {
        Log.d(TAG, "ItemList New mActivatedPosition=" + position);
        mActivatedPosition = position;
    }

    private void setContextTitle(int count) {
        actionMode.setTitle(String.valueOf(count) + " " + (count == 1 ?
                getString(R.string.action_selected_one) :
                getString(R.string.action_selected_many)));
    }

    private void toggleSelection(int position) {
        mAdapter.toggleSelection(position, false);

        int count = mAdapter.getSelectedItemCount();

        if (count == 0) {
            Log.d(TAG, "toggleSelection finish the actionMode");
            actionMode.finish();
        } else {
            Log.d(TAG, "toggleSelection update title after selection count=" + count);
            setContextTitle(count);
            actionMode.invalidate();
        }
    }

    @Override
    public boolean onListItemClick(int position) {
        if (actionMode != null && position != INVALID_POSITION) {
            toggleSelection(position);
            Toast.makeText(getApplicationContext(), "True", Toast.LENGTH_SHORT).show();

            return true;
        } else {
            //Notify the active callbacks interface (the activity, if the
            //fragment is attached to one) that an item has been selected.
            if (mAdapter.getItemCount() > 0) {
                if (position != mActivatedPosition) setActivatedPosition(position);
                Drug drug = mAdapter.getItem(position);
                //TODO: call your custom Callback, for example mCallback.onItemSelected(item.getId());
                ItemDialog.newInstance(drug, position).show(getFragmentManager(), ItemDialog.TAG);
            }
            return false;
        }
    }

    @Override
    public void onListItemLongClick(int position) {
        Toast.makeText(getApplicationContext(), "Long Click", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onListItemLongClick on position " + position);
        if (actionMode == null) {
            Log.d(TAG, "onListItemLongClick actionMode activated!");
            actionMode = startSupportActionMode(this);
        }
        Toast.makeText(this, "LongClick on " + mAdapter.getItem(position).getItem(), Toast.LENGTH_SHORT).show();
        toggleSelection(position);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        //Inflate the correct Menu
        int menuId = R.menu.menu_item_list_context;
        mode.getMenuInflater().inflate(menuId, menu);
        //Activate the ActionMode Multi
        mAdapter.setMode(OrderAdapter.MODE_MULTI);
        if (Utils.hasLollipop()) {
            //getWindow().setNavigationBarColor(getResources().getColor(R.color.colorAccentDark_light));
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorAccent));
        }
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_select_all:
                mAdapter.selectAll();
                setContextTitle(mAdapter.getSelectedItemCount());

                return true;

            case R.id.action_delete:
                for (int i : mAdapter.getSelectedItems()) {
                    //TODO: Remove items from your database. Example:
                    //DatabaseService.getInstance().removeItem(mAdapter.getItem(i));

                }

                //Keep synchronized the Adapter: Remove selected items from Adapter
                String message = (mAdapter.getSelectedItems()) + " " + getString(R.string.action_deleted);
                mAdapter.removeItems(mAdapter.getSelectedItems());

                //Snackbar for Undo
                //noinspection ResourceType
                Snackbar.make(findViewById(R.id.main_view), message, 7000)
                        .setAction(R.string.undo, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAdapter.restoreDeletedItems();
                                //mSwipeHandler.sendEmptyMessage(0);
                            }
                        })
                        .show();
                mAdapter.startUndoTimer(7000L);
                // mSwipeHandler.sendEmptyMessage(1);
                // mSwipeHandler.sendEmptyMessageDelayed(0, OrderAdapter.UNDO_TIMEOUT);
                actionMode.finish();
                return true;

            default:
                return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onDestroyActionMode(ActionMode mode) {
        Log.v(TAG, "onDestroyActionMode called!");
        mAdapter.setMode(OrderAdapter.MODE_SINGLE);
        mAdapter.clearSelection();
        actionMode = null;
        if (Utils.hasLollipop()) {
            //getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark));
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    /**
     * Utility method called from MainActivity on BackPressed
     *
     * @return true if ActionMode was active (in case it is also terminated), false otherwise
     */
    public boolean destroyActionModeIfNeeded() {
        if (actionMode != null) {
            actionMode.finish();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        //If ActionMode is active, back key closes it
        if (destroyActionModeIfNeeded()) return;


        final AlertDialogPro.Builder alert = new AlertDialogPro.Builder(OrderActivity.this);
        alert.setTitle("Exit Order Activity").setIcon(R.mipmap.ic_error_black_24dp).setMessage(R.string.clear_order_item);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Close the App
                //dialogInterface.dismiss();
                mAdapter.removeAllOrders();
                startActivity(new Intent(OrderActivity.this,HomepageActivity.class));
                finish();


            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();

            }
        });
        alert.show();


    }

    @Override
    public void onLoadComplete() {
        mProgressBar.setVisibility(View.INVISIBLE);

    }


}
