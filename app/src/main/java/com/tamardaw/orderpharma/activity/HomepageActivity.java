package com.tamardaw.orderpharma.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.alertdialogpro.AlertDialogPro;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.tamardaw.orderpharma.MainApplication;
import com.tamardaw.orderpharma.R;
import com.tamardaw.orderpharma.adapter.OrderAdapter;
import com.tamardaw.orderpharma.authentication.LoginActivity;
import com.tamardaw.orderpharma.dialog.AboutDialog;
import com.tamardaw.orderpharma.dialog.ItemDialog;
import com.tamardaw.orderpharma.model.Drug;
import com.tamardaw.orderpharma.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomepageActivity extends AppCompatActivity implements OrderAdapter.OnItemClickListener {


    RecyclerView rv;

    List<Drug> drug = new ArrayList<>();
    OrderAdapter adapter;

    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    ParseUser user;
    @Bind(R.id.fab)
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        rv = (RecyclerView) findViewById(R.id.rvhomepage);
        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        adapter = new OrderAdapter(this, "Example parameter for list");
        adapter.removeAllOrders();

        user = ParseUser.getCurrentUser();


        if (user != null) {

            switch (LoginActivity.type) {

                case "Sale":

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("OrderList");
                    query.orderByDescending("createdAt");
                    query.whereEqualTo("sendPerson",user.getUsername());
                    query.findInBackground(new FindCallback<ParseObject>() {

                        @Override
                        public void done(List<ParseObject> orderlist, ParseException e) {

                            if (e == null) {

                                for (ParseObject object : orderlist) {

                                    Drug d = new Drug();
                                    d.item = object.getString("item");
                                    d.generic = object.getString("generic");
                                    d.size = object.getString("size");
                                    d.company = object.getString("company");
                                    d.sendPerson="Me";
                                    drug.add(d);
                                }


                                adapter.addOrders(drug);
                                rv.setAdapter(adapter);

                            } else {
                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    break;

                case "Sale Executive":
                    fab.hide();

                    ParseInstallation installation=ParseInstallation.getCurrentInstallation();
                    installation.put("sale_executive", true);
                    installation.saveInBackground();

                    Toast.makeText(HomepageActivity.this, "Hello Sale Executive", Toast.LENGTH_LONG).show();

                    ParseQuery<ParseObject> query1=ParseQuery.getQuery("OrderList");
                    query1.orderByDescending("createdAt");
                    query1.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> list, ParseException e) {
                            if(e==null){

                                for(ParseObject object: list){

                                    Drug d = new Drug();
                                    d.item = object.getString("item");
                                    d.generic = object.getString("generic");
                                    d.size = object.getString("size");
                                    d.company = object.getString("company");
                                    d.sendPerson=object.getString("sendPerson");
                                    drug.add(d);
                                }

                                adapter.addOrders(drug);
                                rv.setAdapter(adapter);
                            }
                        }
                    });


                    break;

            }
        } else {

            startActivity(new Intent(HomepageActivity.this, LoginActivity.class));
            finish();
        }
    }

/*    private void prepareListData() {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        //Adding child data
        listDataHeader.add("Today");
        listDataHeader.add("Yesterday");
        listDataHeader.add("Others");

        Date d = new Date();


        ParseQuery<ParseObject> query = ParseQuery.getQuery("OrderList");
        query.whereEqualTo("sendPerson", user.toString());
        query.whereEqualTo("createdAt", d);

    }*/

    @OnClick(R.id.fab)
    public void setFab() {

        startActivity(new Intent(HomepageActivity.this, OrderActivity.class));
        finish();
    }


    public void UpdateAtRefresh(){


        rv = (RecyclerView) findViewById(R.id.rvhomepage);
        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        adapter = new OrderAdapter(this, "Example parameter for list");
        adapter.removeAllOrders();

        ParseQuery<ParseObject> query3=ParseQuery.getQuery("OrderList");
        query3.orderByDescending("createdAt");
        query3.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e==null){

                    for(ParseObject object: list){

                        Drug d = new Drug();
                        d.item = object.getString("item");
                        d.generic = object.getString("generic");
                        d.size = object.getString("size");
                        d.company = object.getString("company");
                        d.sendPerson=object.getString("sendPerson");
                        drug.add(d);
                    }

                    adapter.addOrders(drug);
                    rv.setAdapter(adapter);
                }
            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_homepage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        switch (id) {


            case R.id.action_about:

                AboutDialog.newInstance(R.mipmap.ic_info_grey600_24dp,
                        getString(R.string.about_title),
                        getString(R.string.about_body,
                                Utils.getVersionName(this),
                                Utils.getVersionCode(this)))
                        .show(getFragmentManager(), AboutDialog.TAG);

                break;

            case R.id.refresh:

                Toast.makeText(HomepageActivity.this,"Work in progress! To get update data for Sale Executive",Toast.LENGTH_SHORT).show();
                //UpdateAtRefresh();
                break;


            case R.id.logoutUser:

                AlertDialogPro.Builder alert = new AlertDialogPro.Builder(HomepageActivity.this);
                alert.setTitle("Log Out")
                        .setIcon(R.mipmap.ic_exit_to_app_black_24dp)
                        .setMessage("Are you sure to exit?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //logout the user
                                ParseUser.logOut();

                                dialogInterface.dismiss();
                                //take user back to login screen
                                startActivity(new Intent(HomepageActivity.this, LoginActivity.class));
                                finish();

                            }
                        });
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                });
                alert.show();

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onListItemClick(int position) {
        Drug drug = adapter.getItem(position);
        //TODO: call your custom Callback, for example mCallback.onItemSelected(item.getId());
        ItemDialog.newInstance(drug, position).show(getFragmentManager(), ItemDialog.TAG);
        return false;
    }

    @Override
    public void onListItemLongClick(int position) {

    }
}