package com.tamardaw.orderpharma.authentication;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.alertdialogpro.AlertDialogPro;
import com.dd.CircularProgressButton;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.tamardaw.orderpharma.activity.HomepageActivity;
import com.tamardaw.orderpharma.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {


    @Bind(R.id.loginusername)
    MaterialEditText mUserName;

    @Bind(R.id.loginpassword)
    MaterialEditText mPassword;

    @Bind(R.id.btnlogin)
    CircularProgressButton btnLogin;

    ParseUser user;
    public static String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        user=ParseUser.getCurrentUser();

    }

    @OnClick(R.id.btnlogin)
    public void setBtnLogin() {

        btnLogin.setIndeterminateProgressMode(true);

        btnLogin.setProgress(0);
        btnLogin.setProgress(1);

        ParseUser.logInInBackground(mUserName.getText().toString().trim(), mPassword.getText().toString().trim(), new LogInCallback() {

            @Override
            public void done(ParseUser parseUser, ParseException e) {


                if (e == null) {

                    btnLogin.setProgress(100);

                    type=parseUser.getString("type");

                    //success user login
                    Toast.makeText(LoginActivity.this, "Welcome back " + parseUser.getUsername()+" ("+type+")", Toast.LENGTH_LONG).show();

                    //take user to homepage
                    startActivity(new Intent(LoginActivity.this, HomepageActivity.class));
                    finish();

                } else {
                    //sorry there was a problem, advice user

                    btnLogin.setProgress(-1);

                    AlertDialogPro.Builder alert = new AlertDialogPro.Builder(LoginActivity.this);
                    alert.setTitle("Error");
                    alert.setIcon(R.mipmap.ic_error_black_24dp);
                    alert.setMessage(e.getMessage());
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    });
                    alert.show();
                }


            }
        });
    }

    @OnClick(R.id.btncreate)
    public void setBtnCreate() {

        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
