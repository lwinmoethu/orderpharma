package com.tamardaw.orderpharma.authentication;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.alertdialogpro.AlertDialogPro;
import com.dd.processbutton.iml.ActionProcessButton;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.tamardaw.orderpharma.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity {

    @Bind(R.id.signupusername)
    MaterialEditText mUserName;

    @Bind(R.id.signuppassword)
    MaterialEditText mUserPassword;

    @Bind(R.id.signupemail)
    MaterialEditText mUserEmail;

    @Bind(R.id.btnsignup)
    ActionProcessButton btnSignUp;

    @Bind(R.id.spinner1)
    Spinner spinner;

    @BindString(R.string.app_name)
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        //Spinner Item
        ArrayList<String> person = new ArrayList<String>();
        person.add("Sale");
        person.add("Sale Executive");

        //Adapter for spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, person);

        //Bind Data on Spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                //Get Selected Spinner Item
                type = adapterView.getItemAtPosition(i).toString();
                //Toast.makeText(getApplicationContext(), type, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @OnClick(R.id.btnsignup)
    public void setBtnSignUp() {

        btnSignUp.setProgress(20);

        //get the username,password,and email
        final String username = mUserName.getText().toString().trim();
        String password = mUserPassword.getText().toString().trim();
        String email = mUserEmail.getText().toString().trim();

        btnSignUp.setProgress(40);

        //Store user in parse
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.put("type", type);

        btnSignUp.setProgress(60);

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {

                if (e == null) {

                    btnSignUp.setProgress(100);
                    Toast.makeText(SignUpActivity.this, " Sign Up Success, Welcome " + username, Toast.LENGTH_LONG).show();
                    finish();
                } else {

                    AlertDialogPro.Builder alert = new AlertDialogPro.Builder(SignUpActivity.this);
                    alert.setTitle("Error");
                    alert.setIcon(R.mipmap.ic_error_black_24dp);
                    alert.setMessage(e.getMessage());
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    });
                    alert.show();
                    btnSignUp.setProgress(0);

                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
