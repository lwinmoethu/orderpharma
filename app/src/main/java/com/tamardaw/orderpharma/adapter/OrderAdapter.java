package com.tamardaw.orderpharma.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.tamardaw.orderpharma.MainApplication;
import com.tamardaw.orderpharma.R;

import com.tamardaw.orderpharma.activity.OrderActivity;
import com.tamardaw.orderpharma.model.Drug;
import com.tamardaw.orderpharma.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by lwin on 10/21/15.
 */
public class OrderAdapter extends FlexibleAdapter<OrderAdapter.MyViewHolder, Drug> {


    private Context mContext;
    public static List<Drug> drugs = new ArrayList<Drug>();
    private OnItemClickListener mClickListener;
    private LayoutInflater mInflater;
    Drug d;
    OrderActivity activity;

    //Selection fields
    private boolean
            mUserLearnedSelection = false,
            mLastItemInActionMode = false,
            mSelectAll = false;

    public OrderAdapter(Context context) {

        mContext = context;
    }


    public interface OnItemClickListener {
        /**
         * Delegate the click event to the listener and check if selection MULTI enabled.<br/>
         * If yes, call toggleActivation.
         *
         * @param position
         * @return true if MULTI selection is enabled, false for SINGLE selection
         */
        boolean onListItemClick(int position);

        /**
         * This always calls toggleActivation after listener event is consumed.
         *
         * @param position
         */
        void onListItemLongClick(int position);
    }

    public OrderAdapter(Object activity, String listId) {
        super(activity);
        this.mContext = (Context) activity;
        this.mClickListener = (OnItemClickListener) activity;
        updateDataSetAsync(listId);
    }

    @Override
    public void updateDataSet(String param) {
        this.mItems = drugs;

    }

    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (mInflater == null) {
            mInflater = LayoutInflater.from(parent.getContext());
        }

        return new ViewHolder(
                mInflater.inflate(R.layout.order_list, parent, false),
                this);
    }

    @Override
    public void addToParse(int position){

        d=drugs.get(position);

        ParseObject addParse = new ParseObject("OrderList");
        addParse.put("item", d.item);
        addParse.put("generic", d.generic);
        addParse.put("size", d.size);
        addParse.pinInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i(" Success : addToParse", d.item);
            }
        });
    }

    @Override
    public void removeFromParse(int position){
        d=drugs.get(position);

        ParseObject removeParse=new ParseObject("OrderList");
        removeParse.remove("item");
        removeParse.remove("generic");
        removeParse.remove("size");
        removeParse.deleteInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("Success:removeFromParse",d.item);
            }
        });


    }

    public void removeAllFromParse(){


        ParseObject removeAllFromParse=new ParseObject("OrderList");
        removeAllFromParse.unpinInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("Success:UnpinFromParse",drugs.toString());
            }
        });}


    @Override
    public void onBindViewHolder(OrderAdapter.MyViewHolder holder, int position) {
        d = drugs.get(position);
        holder.itemView.setActivated(true);
        holder.Item.setText(d.item);
        holder.Generic.setText(d.generic);
        holder.Size.setText(d.size);
        holder.Company.setText(d.company);
        holder.SendPerson.setText("Sent By :"+d.sendPerson);
        // holder.ImgCancel.setImageResource(R.mipmap.ic_cancel_black_24dp);



        //When user scrolls this bind the correct selection status
        holder.itemView.setActivated(isSelected(position));

        //ANIMATION EXAMPLE!! ImageView - Handle Flip Animation on Select and Deselect ALL
        if (mSelectAll || mLastItemInActionMode) {
            //Reset the flags with delay
            holder.ImgCancel.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSelectAll = mLastItemInActionMode = false;
                }
            }, 200L);
            //Consume the Animation
            //flip(holder.mImageView, isSelected(position), 200L);
        } else {
            //Display the current flip status
            //setFlipped(holder.mImageView, isSelected(position));
        }

        //This "if-else" is just an example
        if (isSelected(position)) {
            holder.ImgCancel.setImageResource(R.mipmap.ic_done_black_24dp);
            holder.Item.setTextColor(Color.RED);
        } else {
            holder.ImgCancel.setImageDrawable(null);
            holder.Item.setTextColor(Color.parseColor("#00796b"));
        }

        //In case of searchText matches with Title or with an Item's field
        // this will be highlighted
        if (hasSearchText()) {
            setHighlightText(holder.Item, d.getItem(), mSearchText);
            setHighlightText(holder.Company, d.getCompany(), mSearchText);
        } else {
            holder.Item.setText(d.getItem());
            holder.Company.setText(d.getCompany());
        }

    }

    private void setHighlightText(TextView textView, String text, String searchText) {
        Spannable spanText = Spannable.Factory.getInstance().newSpannable(text);
        int i = text.toLowerCase(Locale.getDefault()).indexOf(searchText);
        if (i != -1) {
            spanText.setSpan(new ForegroundColorSpan(Utils.getColorAccent(mContext)), i,
                    i + searchText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spanText.setSpan(new StyleSpan(Typeface.BOLD), i,
                    i + searchText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(spanText, TextView.BufferType.SPANNABLE);
        } else {
            textView.setText(text, TextView.BufferType.NORMAL);
        }
    }


    @Override
    public void setMode(int mode) {
        super.setMode(mode);
        if (mode == MODE_SINGLE) mLastItemInActionMode = true;
    }

    @Override
    public void selectAll() {
        mSelectAll = true;
        super.selectAll();
    }

    public void addOrders(List<Drug> drug) {
        drugs.addAll(0, drug);
        notifyItemInserted(0);
    }

    public void removeOrders(int i) {
        drugs.remove(i);
        removeFromParse(i);
        notifyItemRemoved(i);

    }

    public void removeAllOrders() {

        drugs.removeAll(drugs);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return drugs.size();
    }

    @Override
    protected boolean filterObject(Drug myObject, String constraint) {
        String valueText = myObject.getItem();
        //Filter on Title
        if (valueText != null && valueText.toLowerCase().contains(constraint)) {
            return true;
        }
        //Filter on Subtitle
        valueText = myObject.getCompany();
        return valueText != null && valueText.toLowerCase().contains(constraint);
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView Item, Generic, Size, Company,SendPerson;
        public ImageView ImgCancel;
        OrderAdapter orderAdapter;

        public MyViewHolder(View view) {
            super(view);
        }


        public MyViewHolder(View view, OrderAdapter adapter) {
            super(view);
            orderAdapter = adapter;
            Item = (TextView) view.findViewById(R.id.item_list);
            Generic = (TextView) view.findViewById(R.id.generic_list);
            Size = (TextView) view.findViewById(R.id.size_list);
            Company = (TextView) view.findViewById(R.id.company_list);
            SendPerson=(TextView) view.findViewById(R.id.sendPerson);
            ImgCancel = (ImageView) view.findViewById(R.id.cancel_order);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    orderAdapter.mUserLearnedSelection = true;
                    removeOrders(getPosition());
                    Log.i("Removed", getPosition() + "");
                    return false;
                }
            });


        }

    }

    class ViewHolder extends MyViewHolder implements View.OnClickListener, View.OnLongClickListener {


        public ViewHolder(View view, final OrderAdapter adapter) {
            super(view);

            this.orderAdapter = adapter;

            this.Item = (TextView) view.findViewById(R.id.item_list);
            this.Generic = (TextView) view.findViewById(R.id.generic_list);
            this.Size = (TextView) view.findViewById(R.id.size_list);
            this.Company = (TextView) view.findViewById(R.id.company_list);
            this.SendPerson=(TextView) view.findViewById(R.id.sendPerson);
            this.ImgCancel = (ImageView) view.findViewById(R.id.cancel_order);

            this.itemView.setOnClickListener(this);
            this.itemView.setOnLongClickListener(this);

           /* ImgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    removeOrders(getPosition());

                }
            });*/


        }

        @Override
        public void onClick(View view) {

            if (orderAdapter.mClickListener.onListItemClick(getAdapterPosition()))
                toggleActivation();


        }

        @Override
        public boolean onLongClick(View view) {
            orderAdapter.mClickListener.onListItemLongClick(getAdapterPosition());

            //removeOrders(getAdapterPosition());
            toggleActivation();
            return true;
        }

        private void toggleActivation() {
            itemView.setActivated(orderAdapter.isSelected(getAdapterPosition()));
            //This "if-else" is just an example
            if (itemView.isActivated()) {
                ImgCancel.setImageResource(R.mipmap.ic_done_black_24dp);
                this.Item.setTextColor(Color.RED);
            } else {
                ImgCancel.setImageDrawable(null);
                this.Item.setTextColor(Color.parseColor("#00796b"));
            }
            //Example of custom Animation inside the ItemView
            //flip(mImageView, itemView.isActivated());
        }
    }


}
