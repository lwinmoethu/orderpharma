package com.tamardaw.orderpharma;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.tamardaw.orderpharma.database.DBHelper;
import com.tamardaw.orderpharma.model.Drug;

/**
 * Created by lwin on 10/18/15.
 */
public class MainApplication extends Application {

    public static ParseUser currentUser;
    public static ParseInstallation installation;
    public static DBHelper dbHelper;
    public static String[] getItem;
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "3VPenU9W85qCvstF4GCjmjhFagQR7i55nxpF2jIN", "55jTKEnMnrDniidDfqlHTuUlo8ChHaDZX3CU3bTn");
        currentUser=ParseUser.getCurrentUser();
        dbHelper=new DBHelper(getApplicationContext());
        getItem=dbHelper.getItem();

    }
}
