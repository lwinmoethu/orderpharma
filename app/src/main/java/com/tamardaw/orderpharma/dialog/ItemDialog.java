package com.tamardaw.orderpharma.dialog;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tamardaw.orderpharma.R;
import com.tamardaw.orderpharma.model.Drug;

/**
 * Created by lwin on 11/2/15.
 */


public class ItemDialog extends DialogFragment {

    TextView txtItem, txtGeneric, txtSize, txtCompany;

    public static final String TAG = ItemDialog.class.getSimpleName();
    public static final String ARG_ITEM = "item";
    public static final String ARG_ITEM_POSITION = "position";

    public interface OnEditItemListener {
        void onTitleModified(int position, String newTitle);
    }

    private Drug mDrug;
    private int mPosition;

    public ItemDialog() {
    }

    public static ItemDialog newInstance(Drug drug, int position) {
        return newInstance(drug, position, null);
    }

    public static ItemDialog newInstance(Drug item, int position, Fragment fragment) {
        ItemDialog dialog = new ItemDialog();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM, item);
        args.putInt(ARG_ITEM_POSITION, position);
        dialog.setArguments(args);
        dialog.setArguments(args);
        dialog.setTargetFragment(fragment, 0);
        return dialog;
    }

    private OnEditItemListener getListener() {
        OnEditItemListener listener = (OnEditItemListener) getTargetFragment();
        if (listener == null) {
            listener = (OnEditItemListener) getActivity();
        }
        return listener;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ARG_ITEM, mDrug);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.animation_slide_from_right);
    }

    @SuppressLint({"InflateParams", "HandlerLeak"})
    @Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {


        //Pick up bundle parameters
        Bundle bundle;
        if (savedInstanceState == null) {
            bundle = getArguments();
        } else {
            bundle = savedInstanceState;
        }

        mDrug = (Drug) bundle.getSerializable(ARG_ITEM);
        mPosition = bundle.getInt(ARG_ITEM_POSITION);

        //Inflate custom view
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_edit_item, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());//, R.style.AppTheme_AlertDialog);
        builder.setTitle("Item "+(mPosition+1))
                .setView(dialogView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*getListener().onTitleModified(mPosition,
                                txtItem.getText().toString().trim());
                        Utils.hideSoftInputFrom(getActivity(), txtItem);*/
                        dialog.dismiss();
                    }
                });

        final AlertDialog itemDialog = builder.create();

        itemDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                //updateOkButtonState(itemDialog, null);
            }
        });

        txtItem = (TextView) dialogView.findViewById(R.id.txtItem);
        txtGeneric = (TextView) dialogView.findViewById(R.id.txtGeneric);
        txtSize = (TextView) dialogView.findViewById(R.id.txtSize);
        txtCompany = (TextView) dialogView.findViewById(R.id.txtCompany);

        if (mDrug != null) {
            txtItem.setText(mDrug.getItem());
            txtGeneric.setText(mDrug.getGeneric());
            txtSize.setText(mDrug.getSize());
            txtCompany.setText(mDrug.getCompany());
        }
       /* txtItem.requestFocus();


        itemDialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);*/

        return itemDialog;
    }

    /*private void updateOkButtonState(AlertDialog dialog, EditText editText) {
        Button buttonOK = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        if (editText == null || (editText.getText().toString().trim()).length() == 0) {
            buttonOK.setEnabled(false);
            return;
        }
        if (mDrug != null && !mDrug.getItem().equalsIgnoreCase(editText.getText().toString().trim())) {
            buttonOK.setEnabled(true);
        } else {
            editText.setError(getActivity().getString(R.string.app_name));
            buttonOK.setEnabled(false);
        }

        return;
    }
*/


}
