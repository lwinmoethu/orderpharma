package com.tamardaw.orderpharma.dialog;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tamardaw.orderpharma.R;

/**
 * Created by lwin on 11/5/15.
 */
public class AboutDialog extends DialogFragment {

    public static final String TAG = AboutDialog.class.getSimpleName();
    public static final String TAG_ICON = "icon";
    public static final String TAG_TITLE = "title";
    public static final String TAG_MESSAGE = "message";


    public AboutDialog() {
    }

    public static AboutDialog newInstance(int icon, String title, String message) {

        return newInstance(icon, title, message, null);
    }

    public static AboutDialog newInstance(int icon, String title, String message, Fragment fragment) {

        AboutDialog aboutDialog = new AboutDialog();
        Bundle args = new Bundle();
        args.putInt(TAG_ICON, icon);
        args.putString(TAG_TITLE, title);
        args.putString(TAG_MESSAGE, message);
        aboutDialog.setArguments(args);
        if (fragment != null) aboutDialog.setTargetFragment(fragment, 0);

        return aboutDialog;
    }

    @SuppressLint("InflateParams")
    @Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {

        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.about_dialog, null);

        TextView message = (TextView) dialogView.findViewById(R.id.message);
        message.setMovementMethod(LinkMovementMethod.getInstance());
        message.setText(Html.fromHtml(getArguments().getString(TAG_MESSAGE)));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(getArguments().getInt(TAG_ICON))
                .setTitle(getArguments().getString(TAG_TITLE))
                .setView(dialogView)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        final AlertDialog dialog = builder.create();
        return dialog;
    }
}
