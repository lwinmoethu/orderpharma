package com.tamardaw.orderpharma.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tamardaw.orderpharma.model.Drug;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lwin on 10/27/15.
 */
public class DBHelper extends SQLiteOpenHelper {

    List<Drug> info;

    public static String gNeric,cOmpany;

    private static String DB_PATH="/data/data/com.tamardaw.orderpharma/databases";

    private static String DB_NAME="drug_list.db";

    String filepath=DB_PATH+DB_NAME;

    private static SQLiteDatabase DB_Helper;

    private Context mContext;

    public DBHelper(Context context) {
        super(context,DB_NAME,null,1);
        this.mContext=context;

        File f=new File(filepath);

        if(f.exists()){

            Log.i("DataBase","Database Exist");
            openDataBase();
        }else {
            try{
                this.getReadableDatabase();
                copyDataBase();
                Log.i("DataBase", "DataBase copying finished");
                openDataBase();

            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }



    public void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput=mContext.getAssets().open(DB_NAME);
        Log.i("DB myInput : ",DB_NAME);

        //Path to the just created empty db
        String outFileName=DB_PATH+DB_NAME;
        Log.i("DB outFileName : ",outFileName);

        //Open the empty db as the output stream
        OutputStream myOutput=new FileOutputStream(outFileName);
        Log.i("DB myOutput : ",outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer=new byte[1024];
        int length;
        while((length=myInput.read(buffer))>0){
            myOutput.write(buffer,0,length);
        }
        Log.i("DB copy success : ",outFileName);

        //Close the sterams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }


    public void openDataBase() throws SQLException{

        //Open the database
        DB_Helper=SQLiteDatabase.openDatabase(filepath,null,SQLiteDatabase.OPEN_READONLY);
        Log.i("Database","Openig DataBase success");

    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public String[] getItem(){

        Cursor c=DB_Helper.rawQuery("select item from drug_info",null);

        String [] item=new String[c.getCount()];

        int i=0;

        if(c.moveToFirst() && c!=null){

            do{
                String name=c.getString(c.getColumnIndex("item"));
                item[i]=name;
                i++;
            }while (c.moveToNext());
        }

        return item;
    }

    public  List<Drug> getInfoByItem(String item){
        String[] args=new String[1];
        args[0]=item;

        List<Drug>info=new ArrayList<Drug>();

        Cursor c=DB_Helper.rawQuery("select * from drug_info where item=@item", args);


        if(c.moveToFirst()&& c!=null){

            do{
                Drug drug=new Drug();
                drug.id=c.getInt(c.getColumnIndex("_id"));
                drug.item=c.getString(c.getColumnIndex("item"));
                drug.generic=c.getString(c.getColumnIndex("generic"));
                drug.size=c.getString(c.getColumnIndex("size"));
                drug.company=c.getString(c.getColumnIndex("company"));

                info.add(drug);

            }while (c.moveToNext());
        }


        return info;
    }

}
